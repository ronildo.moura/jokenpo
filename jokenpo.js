/* Jogo Pedra, Papel e Tesoura como exercício de arrays e funções em Javascript */
console.log(" >> Jokenpô! <<\n")

// cria dicionário de movimentos e arrow function 'ganhaDe' com condições do jogo 
let movimento = [
	{
		nome : "Pedra",
		dedos : 0,
		ganhaDe : () => (jogador1 == jogador2) ? null :(jogador1 == 0 && jogador2 == 2) ? true : false
	},
	{
		nome : "Papel",
		dedos : 1,
		ganhaDe : () => (jogador1 == jogador2) ? null : (jogador1 == 1 && jogador2 == 0) ? true : false
	},
	{
		nome : "Tesoura",
		dedos : 2,
		ganhaDe : () => (jogador1 == jogador2) ? null : (jogador1 == 2 && jogador2 == 1) ? true : false
	}
];

// gera valor aleatório com a função matemática 'random' e limita com 'floor'
let jogador1 = movimento[Math.floor(Math.random() * 3)].dedos;
let jogador2 = movimento[Math.floor(Math.random() * 3)].dedos;

console.log('Jogador 1 > ' + movimento[jogador1].nome);
console.log('Jogador 2 > ' + movimento[jogador2].nome);

// cria função para jogar e definir o vencedor ou o empate
function jogar(a, b){
	if(movimento[a].ganhaDe() == null || movimento[b].ganhaDe() == null){
		console.log("* Empate!");
	}else if(movimento[a].ganhaDe() == true){
		console.log('* Venceu 1! '+movimento[a].nome+' ganha de '+movimento[b].nome);
	}else {
		console.log('* Venceu 2! '+movimento[b].nome+' ganha de '+movimento[a].nome);
	}
}

jogar(jogador1, jogador2);